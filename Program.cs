﻿using System;
using System.Collections.Generic;

namespace Zoology
{
        class Program
        {
        public static List<Animal> animals = new List<Animal>();
        public static void StartProgram()
        {
            Console.WriteLine("Welcome to the Zoo! Which of these animals would you like to add, Monkey, Bear or Hippo? ");
            string userInput = Console.ReadLine();
            switch (userInput.ToLower())
            {
                case "monkey":
                    animals.Add(Monkey.CreateMonkey());
                    break;
                case "bear":
                    animals.Add(Bear.CreateBear());
                    break;
                case "hippo":
                    animals.Add(Hippo.CreateHippo());
                    break;
                case "print":
                    foreach (Animal animal in animals)
                    {
                        Console.WriteLine(animal.Name);
                    }
                    break;
                default:
                    break;
            }
            EndOfProgram();
        }
        public static void EndOfProgram()
        {
            Console.WriteLine("Do you want to add another animal to the Zoo? Y/N");
            string userAnswer = Console.ReadLine();
            if (userAnswer.ToLower() == "y")
            { 
                StartProgram();
            }
            else
            {
                Console.WriteLine("Thank you for playing this Zoo-game!");
            }
        }
        static void Main(string[] args)
            {
            StartProgram();
            }
        }
    }

