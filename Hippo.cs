﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    class Hippo : Animal
    {
        private bool explosiveDiarrhea;
        public bool ExplosiveDiarrhea { get => explosiveDiarrhea; set => explosiveDiarrhea = value; }

        public Hippo()
        {
            explosiveDiarrhea = true;

        }
        public Hippo(string name, int age, bool explosiveDiarrhea)
        {
            Name = name;
            Age = age;
            ExplosiveDiarrhea = explosiveDiarrhea;
            if (ExplosiveDiarrhea)
            {
                Console.WriteLine("A Hippo has been added to the Zoo, its called " + Name + ",its " + Age + " years old and the first thing it did was to spray the Zoo down with shit!! ");
            }
            else
            {
                Console.WriteLine("A Hippo has been added to the Zoo, its called " + Name + ",its " + Age + " years old");
            }
        }
        public static bool Explosive()
        {
            Console.WriteLine("Does the Hippo have a rumbling noise comming from its tummy? Y/N: ");
            string answer = Console.ReadLine();
            if (answer.ToLower() == "y")
            {

                return true;
            }
            else
            {
                return false;
            }
        }
        public static Hippo CreateHippo()
        {
            string hippoName = Animal.AnimalName("hippo");
            int hippoAge = Animal.AnimalAge("hippo");
            bool exdia = Hippo.Explosive();
            return new Hippo(hippoName, hippoAge, exdia);
        }

    }
}
