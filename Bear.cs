﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    class Bear : Animal
    {
        private string type;
        public string Type { get => type; set => type = value; }

        public Bear()
        {
            type = "Black";
        }

        public Bear(string name, int age, string color)
        {
            Name = name;
            Age = age;
            Type = color;
            Console.WriteLine("A bear has been constructed!");
        }
        public static string SetType()
        {
            Console.WriteLine("What type of bear to you want it to be?: ");
            string bearType = Console.ReadLine();
            return bearType;
        }
        public static Bear CreateBear()
        {
            string bearName = Animal.AnimalName("bear");
            int bearAge = Animal.AnimalAge("bear");
            string bearType = Bear.SetType();

           return new Bear(bearName, bearAge, bearType);

        }


    }
}
