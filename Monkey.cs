﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    class Monkey : Animal
    {
        private int tailLength;
        public int Taillength { get => tailLength; set => tailLength = value; }

        public Monkey()
        {
            tailLength = 10;
        }
        public Monkey(string name, int age, int tailLength)
        {
            Name = name;
            Age = age;
            Taillength = tailLength;
            Console.WriteLine("A Monkey has been constructed with the name " + Name + " he is " + Age + " old, and got a tail with the length of " + Taillength + " cm");
        }
        public static int SetTailLength()
        {
            Console.WriteLine("How long do you want the monkeys tail to be in cm?: ");
            int tailLength = Int32.Parse(Console.ReadLine());
            return tailLength;
        }
        public static Monkey CreateMonkey()
        {
            string monkeyName = Animal.AnimalName("monkey");
            int monkeyAge = Animal.AnimalAge("monkey");
            int monkeyTail = Monkey.SetTailLength();
            return new Monkey(monkeyName, monkeyAge, monkeyTail);
        }
    }
}
