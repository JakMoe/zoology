﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    class Animal
    {
        private string name;
        private int age;

        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }


        public Animal()
        {
            name = "Nameless";
            age = 0;

        }
        public static string AnimalName(string input)
        {
            Console.WriteLine("What name should the " + input + " have?:");
            string animalName = Console.ReadLine();
            return animalName;
        }
        public static int AnimalAge(string input)
        {
            Console.WriteLine("What age is the  " + input + "?:");
            int animalAge = Int32.Parse(Console.ReadLine());
            return animalAge;

        }

    }

}
